# [deprecated] hooktube-persistent

Deprecated: hooktube no longer exists

## Firefox addon that redirects YouTube URLs to hooktube

The following link https://youtube.com/watch?v=S6bOkFLrsAc becomes https://hooktube.com/watch?v=S6bOkFLrsAc

With hooktube

- you keep your data private from the G.
- bypass country blocks and age restrictions.
- download YouTube videos and music.

https://addons.mozilla.org/firefox/addon/hooktube-persistent/

# Installation

You can download it for Firefox via: https://addons.mozilla.org/firefox/addon/hooktube-persistent/
