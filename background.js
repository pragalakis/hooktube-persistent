var pattern = "https://www.youtube.com/*";
var pattern_2 = "https://www.youtu.be/*";

function redirect(requestDetails) {
  // playlists exception
  if (requestDetails.url.includes("/playlist?list")) {
    return 0;
  } else {
    console.log("Redirecting: " + requestDetails.url);
    let last = requestDetails.url.replace(new RegExp(pattern, "g"), "");
    return {
      redirectUrl: `https://www.hooktube.com/${last}`
    };
  }
}

browser.webRequest.onBeforeRequest.addListener(
  redirect,
  { urls: [pattern, pattern_2] },
  ["blocking"]
);
